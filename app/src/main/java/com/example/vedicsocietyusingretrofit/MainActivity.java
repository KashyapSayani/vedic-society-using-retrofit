package com.example.vedicsocietyusingretrofit;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vedicsocietyusingretrofit.Adapter.VedicSocietyListAdapter;
import com.example.vedicsocietyusingretrofit.Interface.VedicSocietyApi;
import com.example.vedicsocietyusingretrofit.model.VedicSocietyModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    RecyclerView rcvUserList;
    ArrayList<VedicSocietyModel> vedicSocietyModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewRefrence();
        getDataFromTheApi();
    }

    void getDataFromTheApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VedicSocietyApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        VedicSocietyApi vedicSocietyApi = retrofit.create(VedicSocietyApi.class);

        Call<List<VedicSocietyModel>> call = vedicSocietyApi.getVedicSocietyData();

        call.enqueue(new Callback<List<VedicSocietyModel>>() {
            @Override
            public void onResponse(Call<List<VedicSocietyModel>> call, Response<List<VedicSocietyModel>> response) {
                List<VedicSocietyModel> vedicSocietyModel = response.body();

                for (VedicSocietyModel vs : vedicSocietyModel) {
                    vedicSocietyModelArrayList.add(vs);
                }
                Log.d("Vedicsociety", vedicSocietyModelArrayList + "");

                rcvUserList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                rcvUserList.setAdapter(new VedicSocietyListAdapter(vedicSocietyModelArrayList));
            }

            @Override
            public void onFailure(Call<List<VedicSocietyModel>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Data Can't Be Fetched", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void initViewRefrence() {
        rcvUserList = findViewById(R.id.rcvUserList);
    }
}