package com.example.vedicsocietyusingretrofit.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VedicSocietyModel implements Serializable {

    @SerializedName("word")
    String Word;
    @SerializedName("nagari")
    String Nagari;
    @SerializedName("description")
    String Description;
    @SerializedName("category")
    String Category;

    public VedicSocietyModel(String word, String nagari, String description, String category) {
        Word = word;
        Nagari = nagari;
        Description = description;
        Category = category;
    }

    public String getWord() {
        return Word;
    }

    public void setWord(String word) {
        Word = word;
    }

    public String getNagari() {
        return Nagari;
    }

    public void setNagari(String nagari) {
        Nagari = nagari;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    @Override
    public String toString() {
        return "VedicSocietyModel{" +
                "Word='" + Word + '\'' +
                ", Nagari='" + Nagari + '\'' +
                ", Description='" + Description + '\'' +
                ", Category='" + Category + '\'' +
                '}';
    }
}
