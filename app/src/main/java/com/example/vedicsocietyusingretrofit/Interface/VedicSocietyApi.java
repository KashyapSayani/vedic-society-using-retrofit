package com.example.vedicsocietyusingretrofit.Interface;

import com.example.vedicsocietyusingretrofit.model.VedicSocietyModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface VedicSocietyApi {

    String BASE_URL = "https://sheetlabs.com/IND/";

    @GET("vs")
    Call<List<VedicSocietyModel>> getVedicSocietyData();

}
